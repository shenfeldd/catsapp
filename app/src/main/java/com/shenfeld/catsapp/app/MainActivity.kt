package com.shenfeld.catsapp.app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.shenfeld.catsapp.R
import com.shenfeld.catsapp.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private val viewModel: MainActivityViewModel by viewModel()
    private var favoritesCount = 0
    private var downloadsCunt = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.onCreate()
        navController = (supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment).navController
        binding.bottomNavigation.setupWithNavController(navController = navController)
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.lastFavBadgeCountLiveData.observe(this) { lastSavedBadgeCount ->
            favoritesCount = lastSavedBadgeCount
            if (favoritesCount != 0) {
                firstSetupFavoritesBadge(id = R.id.favoritesFragment, favoritesCount)
            }
        }

        viewModel.lastDownloadsBadgeCountLiveData.observe(this) { lastSavedBadgeCount ->
            downloadsCunt = lastSavedBadgeCount
            if (downloadsCunt != 0) {
                firstSetupFavoritesBadge(id = R.id.downloadsFragment, downloadsCunt)
            }
        }
    }

    private fun firstSetupFavoritesBadge(id: Int, count: Int) {
        val badge = binding.bottomNavigation.getOrCreateBadge(id)
        badge.isVisible = true
        badge.number = count
    }

    fun setupFavoritesBadge(isAdded: Boolean) {
        if (isAdded) favoritesCount++ else favoritesCount--
        viewModel.saveFavBadgeCount(badgeCount = favoritesCount)

        if (favoritesCount == 0) {
            val badgeDrawable = binding.bottomNavigation.getBadge(R.id.favoritesFragment)
            if (badgeDrawable != null) {
                badgeDrawable.isVisible = false
                badgeDrawable.clearNumber()
                binding.bottomNavigation.removeBadge(R.id.favoritesFragment)
            }
        } else {
            val badge = binding.bottomNavigation.getOrCreateBadge(R.id.favoritesFragment)
            badge.isVisible = true
            badge.number = favoritesCount
        }
    }

    fun setupDownloadsBadge(isAdded: Boolean) {
        if (isAdded) downloadsCunt++ else downloadsCunt--
        viewModel.saveDownloadsBadgeCount(badgeCount = downloadsCunt)

        if (downloadsCunt == 0) {
            val badgeDrawable = binding.bottomNavigation.getBadge(R.id.downloadsFragment)
            if (badgeDrawable != null) {
                badgeDrawable.isVisible = false
                badgeDrawable.clearNumber()
                binding.bottomNavigation.removeBadge(R.id.downloadsFragment)
            }
        } else {
            val badge = binding.bottomNavigation.getOrCreateBadge(R.id.downloadsFragment)
            badge.isVisible = true
            badge.number = downloadsCunt
        }
    }
}