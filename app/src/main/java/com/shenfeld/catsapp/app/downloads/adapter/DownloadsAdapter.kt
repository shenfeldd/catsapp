package com.shenfeld.catsapp.app.downloads.adapter

import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.catsapp.R
import com.shenfeld.catsapp.base.BaseAdapter
import com.shenfeld.catsapp.databinding.ItemImageBinding
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class DownloadsAdapter(private val onDeleteClicked: (ImageItem) -> Unit) : BaseAdapter<ImageItem, DownloadsAdapter.DownloadsVH>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DownloadsVH {
        return DownloadsVH(
            ItemImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DownloadsVH, position: Int) {
        getItem(position = position)?.let {
            holder.bind(item = it)
        }
    }

    inner class DownloadsVH(private val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root),
        Bindable<ImageItem> {

        override fun bind(item: ImageItem) {
            Picasso.get()
                .load(item.uri)
                .transform(RoundedCornersTransformation(10, 0))
                .placeholder(R.drawable.ic_downloaded_unselect)
                .into(binding.ivCat)

            binding.ivCat.setOnLongClickListener {
                val popup = PopupMenu(binding.root.context, binding.ivCat, Gravity.TOP)
                popup.inflate(R.menu.options_menu)
                popup.setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.deleteCat -> {
                            onDeleteClicked.invoke(item)
                            remove(absoluteAdapterPosition)
                            true
                        }

                        else -> {
                            false
                        }
                    }
                }
                popup.show()
                true
            }
        }
    }
}