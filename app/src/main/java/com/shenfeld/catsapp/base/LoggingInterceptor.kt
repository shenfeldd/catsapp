package com.shenfeld.catsapp.base

import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber

class LoggingInterceptor : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        val jsonStr = when {
            message.startsWith("{") -> {
                JSONObject(message).toString(4)
            }
            message.startsWith("[") -> {
                JSONArray(message).toString(4)
            }
            else -> {
                ""
            }
        }

        if (jsonStr.isEmpty()) {
            Timber.tag("OkHttp").d(message)
        } else {
            StringBuilder()
                .appendln("nothing here just to keep indents =)")
                .appendln("=================== JSON BODY START ===================")
                .appendln(jsonStr)
                .appendln("===================  JSON BODY END  ===================")
                .toString().also {
                    Timber.tag("OkHttp").d(it)
                }
        }
    }
}