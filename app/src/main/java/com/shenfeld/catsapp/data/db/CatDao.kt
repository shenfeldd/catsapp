package com.shenfeld.catsapp.data.db

import androidx.room.*
import com.shenfeld.catsapp.data.repositories.models.Cat
import com.shenfeld.catsapp.data.repositories.models.Image

@Dao
interface CatDao {
    @Query("SELECT * FROM cat")
    fun getAll(): List<Cat>

    @Query("SELECT * FROM image")
    fun getAllUris(): List<Image>

    @Query("SELECT * FROM image WHERE image = :uri")
    fun findImage(uri: String): Image

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUri(item: Image)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cat: Cat)

    @Delete
    fun delete(cat: Cat)

    @Delete
    fun deleteUri(image: Image)
}