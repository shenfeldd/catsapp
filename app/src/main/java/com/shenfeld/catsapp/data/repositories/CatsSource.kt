package com.shenfeld.catsapp.data.repositories

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.shenfeld.catsapp.data.repositories.models.Cat

class CatsSource(private val appRepository: AppRepository, private val fromDatabase: Boolean) :
    PagingSource<Int, Cat>() {
    companion object {
        const val COUNT = 10
    }

    override fun getRefreshKey(state: PagingState<Int, Cat>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(it)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Cat> {
        return try {
            val nextPage = params.key ?: 1
            val cats = if (fromDatabase) {
                appRepository.getAllFavorites()
            } else {
                appRepository.getCats(page = nextPage, count = COUNT)
            }

            LoadResult.Page(
                data = cats,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = if (cats.size < COUNT) null else nextPage.plus(1)
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}