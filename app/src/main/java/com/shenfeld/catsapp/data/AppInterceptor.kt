package com.shenfeld.catsapp.data

import com.shenfeld.catsapp.Constants
import okhttp3.Interceptor
import okhttp3.Response

class AppInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
            request = chain
                .request()
                .newBuilder()
                .addHeader("x-api-key", Constants.X_API_KEY)
                .build()
        )
    }
}