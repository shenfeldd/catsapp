package com.shenfeld.stringextensions

object StringExtensions {
    fun String.getFirstChars(): String {
        val firstChars: List<String> = split(" ").filter { it.isNotBlank() }
        return when {
            firstChars.size > 1 -> "${firstChars[0].firstOrNull() ?: ""}${firstChars[1].firstOrNull() ?: ""}"
            firstChars.isNotEmpty() -> (firstChars[0].firstOrNull() ?: "").toString()
            else -> ""
        }
    }

    fun StringBuilder.isFirst() = length == 0
    fun StringBuilder.formatAddress(values: String): String {
        if (isFirst())
            return values
        if (values.isNotBlank())
            return ", $values"
        return ""
    }

    fun String.addZerosToDecimalPart(dp: Int): String {
        if (dotIndex() == -1)
            return ("$this.").addZerosToDecimalPart(dp)
        if (countOfDecimalPart() < dp) {
            return (this + '0').addZerosToDecimalPart(dp)
        }
        return this
    }

    fun String.countOfDecimalPart(): Int =
        when (dotIndex()) {
            -1 -> 0
            else -> length - 1 - dotIndex()
        }

    fun String.dotIndex() = indexOf('.')

    fun String.formatPhoneForSend(): String {
        return if (isNotBlank() && first() == '0') {
            replaceFirst("0", "44").replace(" ", "")
        } else {
            replace(" ", "")
        }
    }
}
