package com.shenfeld.stringextensions

import android.content.Context

class AppExtensions(val context: Context, val isTrue: Boolean) {
    companion object {
        const val TAG = "AppExtensions"
    }
}